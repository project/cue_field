<?php

/**
 * @file
 * Cue Field core functions.
 */

/**
 * The field name for the hms_field inside of a cue field_collection.
 */
define('CUE_FIELD_FIELD_NAME_START', 'cue_field_start');

/**
 * The field name for the title field inside of a cue field_collection.
 */
define('CUE_FIELD_FIELD_NAME_TITLE', 'cue_field_title');

/**
 * Implements hook_menu().
 *
 * Add a form callback as menu local action to each field overview form.
 *
 * @see field_ui_menu()
 */
function cue_field_menu() {
  $items = array();
  // Create local action for all possible entities and bundles.
  foreach (entity_get_info() as $entity_type => $entity_info) {
    if ($entity_info['fieldable']) {
      foreach ($entity_info['bundles'] as $bundle_name => $bundle_info) {
        if (isset($bundle_info['admin'])) {
          // Extract path information from the bundle.
          $path = $bundle_info['admin']['path'];
          // Extract the bundle argument.
          if (isset($bundle_info['admin']['bundle argument'])) {
            $bundle_arg = $bundle_info['admin']['bundle argument'];
          }
          else {
            $bundle_arg = $bundle_name;
          }

          $items["$path/fields/add_cue_field"] = array(
            'title' => 'Add cue field collection',
            'description' => 'Add a cue field collection for the given entity.',
            'type' => MENU_LOCAL_ACTION,
            'page callback' => 'drupal_get_form',
            'page arguments' => array(
              'cue_field_add_cue_field_form', $entity_type, $bundle_arg,
            ),
            'access callback' => 'user_access',
            'access arguments' => array('administer site configuration'),
          );
        }
      }
    }
  }
  return $items;
}

/**
 * Create cue field form.
 *
 * The form allows to enter the name of a new cue field.
 */
function cue_field_add_cue_field_form($form, &$form_state, $entity_type, $entity_bundle) {
  $entity_bundle = field_extract_bundle($entity_type, $entity_bundle);
  $entity_info = entity_get_info($entity_type);

  $form['entity_type'] = array(
    '#type' => 'value',
    '#value' => $entity_type,
  );

  $form['bundle'] = array(
    '#type' => 'value',
    '#value' => $entity_bundle,
  );

  $form['label'] = array(
    '#type' => 'textfield',
    '#title' => t('Label'),
    '#size' => 60,
    '#maxlength' => 128,
    '#required' => TRUE,
    '#description' => t('Human readable label displayed in the ui.'),
  );

  $form['field_name'] = array(
    '#type' => 'machine_name',
    '#maxlength' => 128,
    '#description' => t('A unique machine-readable name containing letters, numbers, and underscores.'),
    '#machine_name' => array(
      'exists' => 'field_info_field',
      'source' => array('label'),
    ),
  );

  $path = explode('/', current_path());
  array_pop($path);

  $title = t('Add new cue field for @bundle (@entity)', array(
    '@entity' => $entity_info['label'],
    '@bundle' => $entity_info['bundles'][$entity_bundle]['label'],
  ));

  return confirm_form($form, $title, implode('/', $path), 'description', 'Add new cue field');
}

/**
 * Form callback for create cue field form.
 *
 * Create a new field collection field and field instance. The field
 * collection is configured to contain a hms_field for a start point and
 * a title field.
 *
 * If a field does not yet exist it will be created.
 */
function cue_field_add_cue_field_form_submit($form, &$form_state) {
  // Get the values we are actually interested in.
  form_state_values_clean($form_state);
  $values = $form_state['values'];

  // Create collection field.
  field_create_field(array(
    'field_name' => $values['field_name'],
    'type' => 'field_collection',
    'cardinality' => -1,
  ));
  field_create_instance(array(
    'field_name' => $values['field_name'],
    'entity_type' => $values['entity_type'],
    'bundle' => $values['bundle'],
    'label' => $values['label'],
  ));

  // Create start field.
  if (is_null(field_info_field(CUE_FIELD_FIELD_NAME_START))) {
    field_create_field(array(
      'field_name' => CUE_FIELD_FIELD_NAME_START,
      'type' => 'hms_field',
    ));
  }
  field_create_instance(array(
    'field_name' => CUE_FIELD_FIELD_NAME_START,
    'entity_type' => 'field_collection_item',
    'bundle' => $values['field_name'],
    'label' => t('Start'),
    'settings' => array('format' => 'm:ss'),
    'required' => TRUE,
  ));

  // Create title field.
  if (is_null(field_info_field(CUE_FIELD_FIELD_NAME_TITLE))) {
    field_create_field(array(
      'field_name' => CUE_FIELD_FIELD_NAME_TITLE,
      'type' => 'text',
    ));
  }
  field_create_instance(array(
    'field_name' => CUE_FIELD_FIELD_NAME_TITLE,
    'entity_type' => 'field_collection_item',
    'bundle' => $values['field_name'],
    'label' => t('Title'),
    'required' => TRUE,
  ));

  // Go back to the field overview form.
  $path = explode('/', current_path());
  array_pop($path);
  drupal_goto(implode('/', $path));
}

/**
 * Implements hook_field_formatter_info().
 *
 * For each supported player a distinct formatter is defined.
 *
 * @todo Instead of creating a formatter for each supported player we
 * might want to have only one formatter and use a setting to define the
 * player.
 */
function cue_field_field_formatter_info() {
  $settings = array(
    'field' => '-none-',
    'edit' => t('Edit'),
    'delete' => t('Delete'),
    'add' => t('Add'),
    'description' => TRUE,
  );

  return array(
    'cue_field_jwplayer' => array(
      'label' => t('Cue point links for jwPlayer'),
      'field types' => array('field_collection'),
      'settings' => $settings,
    ),
    'cue_field_jplayer' => array(
      'label' => t('Cue point links for jPlayer'),
      'field types' => array('field_collection'),
      'settings' => $settings,
    ),
  );
}

/**
 * Implements hook_field_formatter_settings_form().
 */
function cue_field_field_formatter_settings_form($field, $field_instance, $view_mode, $form, &$form_state) {
  $display = $field_instance['display'][$view_mode];
  $settings = $display['settings'];
  $elements = array();

  $elements['edit'] = array(
    '#type' => 'textfield',
    '#title' => t('Edit link title'),
    '#default_value' => $settings['edit'],
    '#description' => t('Leave the title empty, to hide the link.'),
  );
  $elements['delete'] = array(
    '#type' => 'textfield',
    '#title' => t('Delete link title'),
    '#default_value' => $settings['delete'],
    '#description' => t('Leave the title empty, to hide the link.'),
  );
  $elements['add'] = array(
    '#type' => 'textfield',
    '#title' => t('Add link title'),
    '#default_value' => $settings['add'],
    '#description' => t('Leave the title empty, to hide the link.'),
  );
  $elements['description'] = array(
    '#type' => 'checkbox',
    '#title' => t('Show the field description beside the add link.'),
    '#default_value' => $settings['description'],
    '#description' => t('If enabled and the add link is shown, the field description is shown in front of the add link.'),
  );

  return $elements;
}

/**
 * Implements hook_field_formatter_settings_summary().
 */
function cue_field_field_formatter_settings_summary($field, $instance, $view_mode) {
  $display = $instance['display'][$view_mode];
  $settings = $display['settings'];
  $output = array();

  $links = array_filter(array_intersect_key($settings, array_flip(array(
    'add',
    'edit',
    'delete',
  ))));
  if ($links) {
    $output[] = t('Links: @links', array('@links' => check_plain(implode(', ', $links))));
  }
  else {
    $output[] = t('Links: none');
  }

  return implode('<br />', $output);
}

/**
 * Implements hook_field_formatter_view().
 */
function cue_field_field_formatter_view($entity_type, $entity, $field, $instance, $langcode, $items, $display) {
  $element = array();
  $settings = $display['settings'];

  foreach ($items as $delta => $item) {
    if ($field_collection = field_collection_field_get_entity($item)) {
      $title = reset(field_get_items('field_collection_item', $field_collection, CUE_FIELD_FIELD_NAME_TITLE));
      $start = reset(field_get_items('field_collection_item', $field_collection, CUE_FIELD_FIELD_NAME_START));
      $links = array();
      foreach (array('edit', 'delete') as $op) {
        if ($settings[$op] && field_collection_item_access($op == 'edit' ? 'update' : $op, $field_collection)) {
          $links[] = array(
            '#text' => entity_i18n_string("field:{$field['field_name']}:{$instance['bundle']}:setting_$op", $settings[$op]),
            '#path' => $field_collection->path() . '/' . $op,
            '#options' => array('query' => drupal_get_destination()),
          );
        }
      }
      $fragment = 'chapter-' . $field_collection->uuid;
      $uri = entity_uri($entity_type, $entity);
      $uri['options']['fragment'] = $fragment;
      $uri['options']['attributes']['id'] = $fragment;
      $uri['options']['attributes']['data-start'][] = $start['value'];
      $uri['options']['attributes']['data-type'][] = $display['type'];
      // @todo Add data attribute containing the player id allowing us
      // to target pages with multiple player instances on it.
      $element[$delta] = array(
        '#theme' => 'cue_field_jplayer',
        '#title' => $title['safe_value'],
        '#uri' => $uri,
        '#links' => $links,
        '#start' => array(
          '#theme' => 'hms',
          '#format' => 'm:ss',
          '#value' => $start['value'],
        ),
      );
    }
  }
  field_collection_field_formatter_links($element, $entity_type, $entity, $field, $instance, $langcode, $items, $display);

  $path = drupal_get_path('module', 'cue_field') . '/js/' . $display['type'] . '.js';
  $element['#attached']['js'][$path] = array('type' => 'file');
  return $element;
}

/**
 * Implements hook_theme().
 */
function cue_field_theme() {
  return array(
    'cue_field_jplayer' => array(
      'render element' => 'element',
      'file' => 'cue_field.theme.inc',
    ),
  );
}

/**
 * Implements of hook_entity_info_alter().
 *
 * Add uuid just to field_collection_item.
 *
 * @see uuid_entity_info_alter()
 */
function cue_field_entity_info_alter(&$info) {
  $info['field_collection_item']['uuid'] = TRUE;
  $info['field_collection_item']['entity keys']['uuid'] = 'uuid';
}

/**
 * Implements of hook_entity_property_info_alter().
 *
 * @see uuid_entity_property_info_alter()
 */
function cue_field_entity_property_info_alter(&$info) {
  $info['field_collection_item']['properties']['uuid'] = array(
    'label' => t('UUID'),
    'type' => 'text',
    'description' => t('The universally unique ID.'),
    'schema field' => 'uuid',
  );
}

/**
 * Implements of hook_entity_presave().
 *
 * @see uuid_entity_presave
 */
function cue_field_entity_presave($entity, $entity_type) {
  if ($entity_type == 'field_collection_item') {
    if (empty($entity->uuid)) {
      $entity->uuid = cue_field_uuid_generate();
    }
  }
}

/**
 * Implements hook_preprocess_entity().
 */
function cue_field_preprocess_entity(&$variables) {
  // Only do something if we are displaying a field collection.
  if (!isset($variables['elements']['#entity_type']) || $variables['elements']['#entity_type'] != 'field_collection_item') {
    return;
  }

  // If we are displaying a embedded sharedcontent element, replace it with the
  // node content.
  if ($variables['elements']['#theme'] == 'sharedcontent') {
    $host_entity = $variables['elements']['#entity']->hostEntity();

    // Replace the content with the rendered node.
    $variables['content'] = node_view($host_entity, $variables['elements']['#view_mode'], $variables['elements']['#language']);
    unset($variables['content']['#theme']);

    // Use the chapter title for the title.
    if ($title = field_get_items('field_collection_item', $variables['elements']['#entity'], CUE_FIELD_FIELD_NAME_TITLE)) {
      $variables['title'] = check_plain($title[0]['value']);
    }
  }
}

/**
 * Implements hook_sharedcontent_richmedia().
 */
function cue_field_sharedcontent_richmedia() {
  $plugins['cue_field'] = array(
    'label' => t('Cue Field'),
    'handler' => array(
      'class' => 'CueFieldRichMedia',
    ),
  );
  return $plugins;
}

/**
 * Generates a Universally Unique IDentifier (UUID).
 *
 * See Drupal 8  Drupal\Component\Uuid\Php\Php::generate()
 *
 * @return string
 *   A 32 byte integer represented as a hex string formatted with 4 hypens.
 */
function cue_field_uuid_generate() {
  $hex = substr(hash('sha256', drupal_random_bytes(16)), 0, 32);

  // The field names refer to RFC 4122 section 4.1.2.
  $time_low = substr($hex, 0, 8);
  $time_mid = substr($hex, 8, 4);

  $time_hi_and_version = base_convert(substr($hex, 12, 4), 16, 10);
  $time_hi_and_version &= 0x0FFF;
  $time_hi_and_version |= (4 << 12);

  $clock_seq_hi_and_reserved = base_convert(substr($hex, 16, 4), 16, 10);
  $clock_seq_hi_and_reserved &= 0x3F;
  $clock_seq_hi_and_reserved |= 0x80;

  $clock_seq_low = substr($hex, 20, 2);
  $nodes = substr($hex, 20);

  $uuid = sprintf('%s-%s-%04x-%02x%02x-%s', $time_low, $time_mid, $time_hi_and_version, $clock_seq_hi_and_reserved, $clock_seq_low, $nodes);

  return $uuid;
}
